package Desafios;
import java.util.*;



public class Desafio10 {
	public static Scanner entrada=new Scanner(System.in);
	 
	  public static void main(String[] args) {
	    System.out.println("El siguiente juego consta en adivinar un n�mero");
	 
	    
	    int num=(int)(Math.random()*100);
	 
	    System.out.println("\nIngrese un n�mero entre  0 y 100");
	 
	    int entra=-1;
	    int cont=0;
	 
	    
	    do {
	 
	      
	      entra=obtenerValor();
	      if(entra>num) {
	        System.out.println("\nEl n�mero ingresado no es correcto, el n�mero es menor");
	      }else if(entra<num) {
	        System.out.println("\nEl n�mero ingresado no es correcto, el n�mero es mayor");
	      }
	      cont++;
	    } while(entra!=num);
	 
	    System.out.println("\nIngresaste de manera correca el n�mero en " + cont + " intentos.");
	  }
	 
	  public static int obtenerValor() {
	    int valor=0;
	 
	    while(true){
	      try {
	        System.out.print("\nIngrese el n�mero: ");
	        valor=entrada.nextInt();
	 
	        if(valor<0 || valor>100) {
	          System.out.println("\nEl n�mero ingresado tiene que estar entre el 0 y el 100");
	        }else{
	    
	          break;
	        }
	      }catch(InputMismatchException e) {
	        
	        System.out.println("\nEl valor tiene que ser numerico...");
	        entrada.nextLine();
	      }
	    }
	    return valor;
	  }
}
