package Desafio11;

public class cuenta {

	    public static void main(String args[]) {
	        CuentaCorriente Cuenta1 = new CuentaCorriente("Cuenta1", 2000);
	        CuentaCorriente Cuenta2 = new CuentaCorriente("Cuenta2", 2500);
	        CuentaCorriente Cuenta3 = new CuentaCorriente("Cuenta3", 500);
	        
	        Cuenta1.setNumeroCuenta(001122);
	        Cuenta2.setNumeroCuenta(001133);

	        System.out.println("El primer usuario se llama " + Cuenta1.getNombreTitular() + " y el saldo de su cuenta es: $" + Cuenta1.getSaldo());
	        System.out.println("El primer usuario se llama " + Cuenta2.getNombreTitular() + " y el saldo de su cuenta es: $" + Cuenta2.getSaldo());
	        
	        Cuenta3.Transferir(Cuenta1, Cuenta2, 2000);
	        
	        System.out.println("El saldo de "+Cuenta1.getNombreTitular()+" es ahora: $"+Cuenta1.getSaldo());
	        System.out.println("El saldo de "+Cuenta2.getNombreTitular()+" es ahora: $"+Cuenta2.getSaldo());

	        System.out.println(Cuenta1.mostrarDatos());
	        System.out.println(Cuenta2.mostrarDatos());
	    }
	}


